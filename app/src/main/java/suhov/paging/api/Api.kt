package suhov.paging.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import suhov.paging.api.model.StackResponse

/** API -> https://api.stackexchange.com */

interface Api {

    @GET("answers")
    fun getAnswers(
        @Query("page") page: Int,
        @Query("pagesize") size: Int,
        @Query("site") site: String
    ): Call<StackResponse>
}