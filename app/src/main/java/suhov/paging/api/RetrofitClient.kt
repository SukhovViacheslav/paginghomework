package suhov.paging.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import suhov.paging.api.model.StackResponse
import suhov.paging.util.AppConfig.BASE_URL
import suhov.paging.util.Constants.PAGE_SIZE
import suhov.paging.util.Constants.SITE_NAME

class RetrofitClient {

    private val interceptor = HttpLoggingInterceptor()
    private val retrofit: Retrofit
    private val api:Api

    init {

        interceptor.level = HttpLoggingInterceptor.Level.BODY

        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(Api::class.java)
    }

    fun getPage(pageIndex: Int): Call<StackResponse> = api.getAnswers(pageIndex, PAGE_SIZE, SITE_NAME)
}