package suhov.paging.api.model

class Owner {
    var reputation: Int = 0
    var user_id: Long = 0
    var user_type: String? = null
    var accept_rate: Int = 0
    var profile_image: String? = null
    var display_name: String? = null
    var link: String? = null
}