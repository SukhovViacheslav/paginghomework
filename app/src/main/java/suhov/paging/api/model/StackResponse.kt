package suhov.paging.api.model

class StackResponse {
    var items: List<Item>? = null
    var has_more: Boolean = false
    var backoff: Int = 0
    var quota_max: Int = 0
    var quota_remaining: Int = 0
}