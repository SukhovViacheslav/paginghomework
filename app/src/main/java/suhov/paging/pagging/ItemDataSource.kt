package suhov.paging.pagging

import android.util.Log
import androidx.paging.PageKeyedDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import suhov.paging.api.RetrofitClient
import suhov.paging.api.model.Item
import suhov.paging.api.model.StackResponse
import suhov.paging.util.Constants.FIRST_PAGE

// Постраничная загрузка, поэтому PageKeyedDataSource
class ItemDataSource : PageKeyedDataSource<Int, Item>() {

    // Первоночальная загрузка
    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Item>) {

        // params.requestedLoadSize - сколько данных грузить
        // params.placeholdersEnabled - включены ли placeholders

        RetrofitClient().getPage(FIRST_PAGE).enqueue(object : Callback<StackResponse> {
            override fun onResponse(call: Call<StackResponse>, response: Response<StackResponse>) {
                if (response.isSuccessful) {

                    // callback.onResult() - передаем данные и указываем ключи для загрузки предыдущей и последующей порций данных.
                    // Если вместо ключа мы передаем null, то PagedList понимает, что в том направлении данных нет.
                    // Соответственно, previousPageKey чаще всего будет null, т.к. мы грузим данные с начала и предыдущих порций нет.
                    // Если Placeholders включены можно использовать второй вариант onResult, но нужно указать общее число записей
                    // и позицию, с которой эти данные начинаются.
                    callback.onResult(response.body()!!.items!!, null, FIRST_PAGE + 1)
                }
            }

            override fun onFailure(call: Call<StackResponse>, t: Throwable) {
                Log.e(TAG, "loadInitial: $t")
            }
        })

    }

    // Загрузка загрузка предыдущей порции данных. Он вызывается при прокрутке списка вверх, если указан previousPageKey
    // при вызове колбэка в loadInitial. Но так как previousPageKey = null метод loadBefore не нуждается в реализации, т.к. он просто не вызывается.
    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {}

    // Загрузка следующей порции данных. Он вызывается при прокрутке списка вниз, если указан nextPageKey при вызове колбэка в loadInitial.
    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {

        RetrofitClient().getPage(params.key).enqueue(object : Callback<StackResponse> {
            override fun onResponse(call: Call<StackResponse>, response: Response<StackResponse>) {
                if (response.isSuccessful) {

                    // onResult(List<Value> data, Key adjacentPageKey) - передаем данные и указываем какой ключ
                    // использовать для получения очередной следующей порции. Если в качестве ключа передать null, то
                    // PagedList поймет, что в этом направлении данных больше нет.

                    // Иногда сервер возвращает страницу, но не возвращет номер следующей
                    val key = if (response.body()!!.has_more) params.key + 1 else null

                    callback.onResult(response.body()!!.items!!, key)
                }
            }

            override fun onFailure(call: Call<StackResponse>, t: Throwable) {
                Log.e(TAG, "loadAfter: $t")
            }
        })
    }

    private companion object { private val TAG: String = ItemDataSource::class.java.simpleName }
}