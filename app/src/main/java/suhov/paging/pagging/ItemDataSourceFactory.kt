package suhov.paging.pagging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import suhov.paging.api.model.Item

class ItemDataSourceFactory: DataSource.Factory<Int, Item>() {

    val dataSourceLiveData: MutableLiveData<PageKeyedDataSource<Int,Item>> = MutableLiveData()

    override fun create(): DataSource<Int, Item> {
        val dataSource = ItemDataSource()

        dataSourceLiveData.postValue(dataSource)
        return dataSource
    }
}