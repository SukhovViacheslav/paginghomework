package suhov.paging.util

object Constants {
      const val PAGE_SIZE = 10
      const val FIRST_PAGE = 1
      const val SITE_NAME = "stackoverflow"
}