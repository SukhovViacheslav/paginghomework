package suhov.paging.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/** ДЗ: PAGING LIBRARY - Пользователи с сайта Stack Overflow
 *  API -> https://api.stackexchange.com */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(MainView(context = this@MainActivity))
    }
}