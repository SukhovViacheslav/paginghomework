package suhov.paging.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_view.view.*
import suhov.paging.R
import suhov.paging.util.NetworkUtil
import suhov.paging.viewmodel.ItemViewModel

class MainView(context: Context) : FrameLayout(context) {

    private val activity = context as MainActivity
    private val viewModel = ViewModelProviders.of(activity).get(ItemViewModel::class.java)

    init {
        LayoutInflater.from(context).inflate(R.layout.main_view, this@MainView)

        recycler.layoutManager = LinearLayoutManager(context)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.setHasFixedSize(true)

        show()
        // fab.setOnClickListener { show() }
    }

    private fun show() {
        if (NetworkUtil(context).isConnected()) {

            val adapter = PagingListAdapter()

            viewModel.observePagedList(activity, Observer { adapter.submitList(it) })
            recycler.adapter = adapter

        } else Toast.makeText(context, "Network is disconnect!", Toast.LENGTH_SHORT).show()
    }
}