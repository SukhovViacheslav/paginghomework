package suhov.paging.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import suhov.paging.api.model.Item
import suhov.paging.pagging.ItemDataSourceFactory
import suhov.paging.util.Constants.PAGE_SIZE

class ItemViewModel : ViewModel() {

    private var itemPagedList: LiveData<PagedList<Item>>
    private var liveDataSource: LiveData<PageKeyedDataSource<Int, Item>>

    init {

        val factory = ItemDataSourceFactory()
        liveDataSource = factory.dataSourceLiveData

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)        // Отключаем Placeholders, т.к. размер не фиксированный
            .setPageSize(PAGE_SIZE)              // Подгржаем по 10 штук
            .build()

        itemPagedList = LivePagedListBuilder(factory, config).build()
    }

    fun observePagedList(owner: LifecycleOwner, observer: Observer<PagedList<Item>>) {
        itemPagedList.observe(owner, observer)
    }

}